# Entrez Fetcher

## Install

`pip install git+https://gitlab.com/celliern/entrez_fetch`

## Usage

Better to provide the e-mail and ncbi token, but not mandatory (allow to retrieve more data at
once).

```python3
>>> from entrez_fetcher import EntrezFetcher
>>> fetcher = EntrezFetcher(email="...", api_key="...")
>>> fetcher.accession_to_summary("NC_009615")
          Item         Id    Caption  ... ReplacedBy Comment  AccessionVersion
NC_009615   []  150006674  NC_009615  ...                          NC_009615.1

[1 rows x 15 columns]
```

Work for one accesion id or a sequence of accession ids.

Available:

```python3
fetcher.accession_to_fasta(accession_numbers: Union[str, Sequence[str]]) -> Sequence[Tuple[str, Bio.SeqRecord]]  # return sequence of tuple (accession_id, biopython seq_record)
fetcher.accession_to_genbank(accession_numbers: Union[str, Sequence[str]]) -> Sequence[Tuple[str, Bio.SeqRecord]]  # return sequence of tuple (accession_id, biopython seq_record)
fetcher.accession_to_summary(accession_numbers: Union[str, Sequence[str]]) -> pd.DataFrame # Entrez esummary entry as pandas dataframe
fetcher.taxid_to_taxonomy(taxids: Union[str, Sequence[str]]) -> pd.DataFrame
fetcher.accession_to_taxonomy(accession_numbers: Union[str, Sequence[str]]) -> pd.DataFrame
```
